CREATE TABLE client (
	id int AUTO_INCREMENT PRIMARY KEY,
	full_name varchar(128),
	abbreviation varchar(6)
);
