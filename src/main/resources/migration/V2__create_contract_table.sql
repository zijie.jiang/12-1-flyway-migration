CREATE TABLE contract (
	id int AUTO_INCREMENT PRIMARY KEY,
	client_id int,
	name VARCHAR(256),
	FOREIGN KEY (client_id) REFERENCES client (id)
);
