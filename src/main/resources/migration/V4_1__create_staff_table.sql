CREATE TABLE staff (
	id int AUTO_INCREMENT PRIMARY KEY,
	firstname VARCHAR(32),
	lastname VARCHAR(16),
	office_id int,
	FOREIGN KEY (office_id) REFERENCES office (id)
);
