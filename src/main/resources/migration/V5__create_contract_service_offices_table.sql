CREATE TABLE contract_service_offices (
	office_id int,
	contract_id int,
	PRIMARY KEY (office_id, contract_id),
	KEY office_id (office_id),
	CONSTRAINT office_id FOREIGN KEY (office_id) REFERENCES office (id),
	KEY contract_id (contract_id),
	CONSTRAINT contract_id FOREIGN KEY (contract_id) REFERENCES contract (id)
);
